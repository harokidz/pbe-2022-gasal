<?php
/**
 *Created by $(USER) on $(DATE).
 */

namespace App\Model;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class Auth
{
    public static function verify($username, $password)
    {
        return DB::table('users')
            ->where('us_email', $username)
            ->first();
    }

    public static function createToken($userId)
    {
        $token = Str::uuid();
        $expired = date(
            'Y-m-d H:i:s',
            strtotime(now() . ' +30 minute')
        );
        $tokens = [
            'tk_token' => $token,
            'tk_expired' => $expired,
            'tk_us_id' => $userId
        ];
        $result = DB::table('tokens')
            ->insert($tokens);
        if ($result) {
            return $token;
        }
        return null;
    }
}
