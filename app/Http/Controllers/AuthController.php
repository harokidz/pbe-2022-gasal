<?php

namespace App\Http\Controllers;

use App\Exceptions\NotAuthException;
use App\Model\Auth;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psy\Util\Str;

class AuthController extends Controller
{
    public function verify()
    {
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        $user = Auth::verify($username, $password);
        if ($user !== null) {
            #verifikasi password
            if (password_verify($password, $user->us_password)) {
                #kondisi password benar
                $token = Auth::createToken($user->us_id);
                if ($token !== null) {
                    return response()->json([
                        'email' => $user->us_email,
                        'name' => $user->us_name,
                        'token' => $token
                    ]);
                }
            }
        }
        throw new NotAuthException();
//        return response()->json([], 401);
    }

    public function cobaDBTransaksi()
    {
        #mulai database transaction
        DB::beginTransaction();
        try {
            #insert data user
            $usId = DB::table('users')->insertGetId([
                'us_name' => 'Gunindo',
                'us_email' => 'gunindo@mail.com',
                'us_password' => password_hash('admin', PASSWORD_DEFAULT)
            ]);
            $expired = date(
                'Y-m-d H:i:s',
                strtotime(now() . ' +30 minute')
            );
            $token = \Illuminate\Support\Str::uuid();
            DB::table('tokens')->insert([
                'tk_token' => $token,
                'tk_expired' => $expired,
                'tk_us_id' => $usId
            ]);
            #jika tidak terjadi error, proses semua query
            DB::commit();
            return response()->json([
                'message' => "User Berhasil disimpan",
                'user' => [
                    'name' => 'Gunindo',
                    'email' => 'gunindo@mail.com',
                    'token' => $token
                ]
            ]);
        } catch (\Exception $e) {

            #batalkan semua query jika terjadi error
            DB::rollBack();
            return \response()->json(['message' => "User Gagal disimpan"]);
        }
    }
}
