<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\PbeBaseController;
use App\Model\Product;

class ProductController extends PbeBaseController
{

    public function index()
    {
        $products = Product::where('pr_deleted_at', null)
            ->get();
        return response()->json($products, 200);
    }

    public function store()
    {
        $this->isAdmin();
        $prCode = request('code');
        $prName = request('name');
        $prPrice = request('price');

        #periksa apakah ada data yang kosong
        #1.Variabel untuk menampung semua error
        $errors = [];
        #2.Cek apakah ada yang kosong
        if (empty($prCode)) {
            $errors[] = "Kode Produk tidak boleh kosong";
        }
        if (empty($prName)) {
            $errors[] = "Nama Produk tidak boleh kosong";
        }
        if (empty($prPrice) || $prPrice <= 0) {
            $errors[] = "Harga Produk tidak boleh kosong dan harus lebih besar dari 0";
        }
        #3 cek jika kode sudah digunakan
        $product = Product::where("pr_code", $prCode)->first();
        if ($product !== null) {
            $errors[] = "Kode Produk $prCode sudah digunakan";
        }
        if (count($errors) > 0) {
            return \response()->json(["errors" => $errors], 400);
        }

        $productRequest = [
            'pr_code' => $prCode,
            'pr_name' => $prName,
            'pr_price' => $prPrice,
        ];

        $productId = Product::insertGetId($productRequest);
        $productResponse = Product::find($productId);

        return response()->json($productResponse, 201);
    }

    public function getById($productId)
    {
        $productResponse = Product::where('pr_id', $productId)
            ->where('pr_deleted_at', null)->first();
        if ($productResponse === null) {
            return response()->json(["error" => "Not Found"], 404);
        }
        return response()->json($productResponse, 200);
    }

    public function update($productId)
    {
        $this->isSuperadmin();
        #ambil data dari request body
        $prName = request('name');
        $prPrice = request('price');
        $prStock = request('stock');

        #populate data sesuai dengan database
        $productRequest = [
            'pr_name' => $prName,
            'pr_price' => $prPrice,
            'pr_stock' => $prStock,
        ];

        #proses update
        Product::where('pr_id', $productId)
            ->update($productRequest);

        #ambil data terbaru dan kembalikan
        $productResponse = Product::find($productId);
        return response()->json($productResponse);
    }

    public function softDelete($productId)
    {
        $this->isSuperadmin();
        #proses update
        Product::where('pr_id', $productId)
            ->update(["pr_deleted_at" => now()]);
        return \response()
            ->json(['message' => "Data Berhasil dihapus"]);
    }

    public function delete($productId)
    {
        $this->isSuperadmin();
        #proses update
        Product::deleteById($productId);
        return \response()
            ->json(['message' => "Data Berhasil dihapus"]);
    }

    public function restore($productId)
    {
        $this->isSuperadmin();
        #proses update
        Product::where('pr_id', $productId)
            ->update(["pr_deleted_at" => null]);
        return \response()
            ->json(['message' => "Data Berhasil di-restore"]);
    }

}
