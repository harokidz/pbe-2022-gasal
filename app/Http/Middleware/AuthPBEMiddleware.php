<?php

namespace App\Http\Middleware;

use App\Exceptions\NotAuthException;
use Closure;

class AuthPBEMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('pbe-token'))) {
            throw new NotAuthException();
        }
        return $next($request);
    }
}
