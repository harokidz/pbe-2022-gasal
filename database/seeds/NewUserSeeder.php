<?php

use Illuminate\Database\Seeder;

class NewUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'us_name' => 'Snow Man',
            'us_email' => 'snowman@mail.com',
            'us_password' => password_hash('admin', PASSWORD_DEFAULT)
        ]);
        DB::table('users')->insert([
            'us_name' => 'Epson',
            'us_email' => 'epson@mail.com',
            'us_password' => password_hash('admin', PASSWORD_DEFAULT)
        ]);
    }
}
