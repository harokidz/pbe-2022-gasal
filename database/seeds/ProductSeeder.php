<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'pr_code' => 'PR001',
            'pr_name' => 'Mouse Logitech',
            'pr_price' => 98000,
            'pr_create_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('products')->insert([
            'pr_code' => 'PR002',
            'pr_name' => 'Spidol Snowman Hitam',
            'pr_price' => 12000,
            'pr_create_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('products')->insert([
            'pr_code' => 'PR003',
            'pr_name' => 'LG TV 60 inch',
            'pr_price' => 6500000,
            'pr_create_at' => date('Y-m-d H:i:s')
        ]);
    }
    #php artisan db:seed --class=ProductSeeder
}
