<?php

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    $response = Http::withOptions(["verify"=>false])
        ->get('https://www.themealdb.com/api/json/v1/1/filter.php?i=chicken_breast');

    $meals = json_decode($response->body());
    $table = "<table border='1'>";
    foreach ($meals->meals as $meal){
        $table .= "<tr>";
        $table .= "<td>".$meal->strMeal."</td>";
        $table .= "<td>".$meal->idMeal."</td>";
        $table .= "</tr>";
    }
    $table .= "</table>";
    echo $table;
});
